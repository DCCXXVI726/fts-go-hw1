package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func main() {
	const (
		highestPrice = 0
		buyTime      = 1
		sellTime     = 2
	)

	companies, err := OpenCandles("candles_5m.csv")
	if err != nil {
		log.Fatalf("Can't open candles: %s", err)
	}

	bestValue, err := GetArrayMaxValue(companies)
	if err != nil {
		log.Fatalf("Can't get array of max value: %s", err)
	}

	userTrade, err := ReadTrades("user_trades.csv")
	if err != nil {
		log.Fatalf("Can't read trades: %s", err)
	}

	file, err := os.Create("result.csv")
	if err != nil {
		log.Fatalf("can't create file result.csv: %s", err)
	}
	defer file.Close()
	writer := csv.NewWriter(file)

	for user, compMap := range userTrade {
		for comp, value := range compMap {
			max, ok := bestValue[comp]
			if !ok {
				log.Fatalf("can't find company %s in candles", comp)
			}

			vmax, err := strconv.ParseFloat(max[highestPrice], 64)
			if err != nil {
				log.Fatalf("can't parse highest price %s in company %s: %s", max[highestPrice], comp, err)
			}

			err = writer.Write([]string{user, comp, fmt.Sprintf("%.2f", value), fmt.Sprintf("%.2f", vmax), fmt.Sprintf("%.2f", vmax-value), max[buyTime], max[sellTime]})
			if err != nil {
				err = fmt.Errorf("can't write on user %s in company %s: %s", user, comp, err)
				log.Fatal(err)
			}

			writer.Flush()
		}
	}
}

func GetArrayMaxValue(companies map[string][][]string) (map[string][]string, error) {
	var err error

	bestValue := make(map[string][]string)

	for company, arrays := range companies {
		bestValue[company], err = FindMaxValue(arrays)
		if err != nil {
			return nil, fmt.Errorf("can't find max value in %s: %s", company, err)
		}
	}

	return bestValue, nil
}

func ReadTrades(name string) (map[string]map[string]float64, error) {
	var record []string

	const (
		id             = 0
		ticker         = 2
		buy            = 3
		time           = 5
		numberOfParams = 5
	)

	userTrade := make(map[string]map[string]float64)

	file, err := os.Open(name)
	if err != nil {
		return nil, fmt.Errorf("can't open file %s: %s", name, err)
	}

	defer file.Close()
	reader := csv.NewReader(file)

	for {
		record, err = reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			return nil, fmt.Errorf("can't read file %s: %s", name, err)
		}

		if len(record) != numberOfParams {
			return nil, fmt.Errorf("wrong number of params in %s", name)
		}

		value, err := GetValue(record[buy:time])
		if err != nil {
			return nil, fmt.Errorf("can't get value from %s: %s", record[buy], err)
		}

		compMap, ok := userTrade[record[id]]
		if !ok {
			compMap = make(map[string]float64)
			compMap[record[ticker]] = value
			userTrade[record[id]] = compMap
		} else {
			innerValue, inok := compMap[record[ticker]]
			if !inok {
				compMap[record[ticker]] = value
			} else {
				compMap[record[ticker]] = innerValue + value
			}
		}
	}

	return userTrade, nil
}

func GetValue(record []string) (float64, error) {
	var (
		value  float64
		sign   float64
		err    error
		svalue string
	)

	const (
		buy  = 0
		sell = 1
	)

	if record == nil {
		return 0, fmt.Errorf("string is empty")
	}

	if record[sell] == "0" {
		svalue = record[buy]
		sign = 1
	} else {
		svalue = record[sell]
		sign = -1
	}

	value, err = strconv.ParseFloat(svalue, 64)
	if err != nil {
		return 0, fmt.Errorf("can't parse sell value %s: %s", svalue, err)
	}

	value *= sign

	return value, nil
}

func OpenCandles(name string) (map[string][][]string, error) {
	var record []string

	const (
		id             = 0
		time           = 1
		numberOfParams = 6
	)

	companies := make(map[string][][]string)

	file, err := os.Open(name)
	if err != nil {
		return nil, fmt.Errorf("can't open file %s: %s", name, err)
	}

	defer file.Close()
	reader := csv.NewReader(file)

	for {
		record, err = reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			return nil, fmt.Errorf("can't read file %s: %s", name, err)
		}

		if len(record) != numberOfParams {
			return nil, fmt.Errorf("wrong number of params in %s", name)
		}

		value, ok := companies[record[id]]
		if !ok {
			value = make([][]string, 0)
			companies[record[id]] = append(value, record[time:])
		} else {
			companies[record[id]] = append(value, record[time:])
		}
	}

	return companies, nil
}

func FindMaxValue(params [][]string) ([]string, error) {
	const (
		time         = 0
		highestPrice = 2
		lowestPrice  = 3
		sellTime     = 1
		buyTime      = 2
		maxValue     = 0
	)

	high := make([]float64, len(params))
	low := make([]float64, len(params))

	for i := 0; i < len(params); i++ {
		vhigh, err := strconv.ParseFloat(params[i][lowestPrice], 64)
		if err != nil {
			return nil, fmt.Errorf("can't parse highest price %s: %s", params[i][highestPrice], err)
		}

		high[i] = vhigh

		vlow, err := strconv.ParseFloat(params[i][lowestPrice], 64)
		if err != nil {
			return nil, fmt.Errorf("can't parse lowest price %s: %s", params[i][lowestPrice], err)
		}

		low[i] = vlow
	}

	max := high[0] - low[0]
	sellIndex := 0
	buyIndex := 0

	for i := 0; i < len(params); i++ {
		locmax := high[i] - low[0]
		locSellIndex := i
		locBuyIndex := 0

		for k := 1; k <= i; k++ {
			if high[i]-low[k] > locmax {
				locmax = high[i] - low[k]
				locSellIndex = i
				locBuyIndex = k
			}
		}

		if locmax > max {
			max = locmax
			sellIndex = locSellIndex
			buyIndex = locBuyIndex
		}
	}

	state := make([]string, 3)
	state[maxValue] = fmt.Sprintf("%f", max)
	state[sellTime] = params[sellIndex][time]
	state[buyTime] = params[buyIndex][time]

	return state, nil
}
